#!/usr/bin/env sh

export LISTEN_PORT="${LISTEN:-18443}"
export LISTEN_HOST="${LISTEN_HOST:-0.0.0.0}"
export MINIKUBE_API_HOST="${MINIKUBE_API_HOST:-minikube}"
export MINIKUBE_API_PORT="${MINIKUBE_API_PORT:-8443}"

echo "Generating config from template"
envsubst < nginx.tmpl > /etc/nginx/nginx.conf

echo "Starting nginx"
nginx