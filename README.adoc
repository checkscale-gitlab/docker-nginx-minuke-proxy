:toc:
= nginx minikube proxy

This Docker image allows using nginx as proxy to expose the minikube api.
Instead of starting nginx directly as the default nginx does, it uses a `start.sh` that applies ENV variables to the our default nginx config.

You can find the image on Docker hub at https://hub.docker.com/repository/docker/chevdor/nginx-minikube-proxy/general

== Build

    docker build -t chevdor/nginx-minikube-proxy .

== Run

=== Basic run

    docker run -d -it --network minikube --name minikube-proxy --restart unless-stopped -p 18443:18443 chevdor/nginx-minikube-proxy

WARNING: It is critical to include the `--network minikube` part

=== Passing options

You may pass the following options:

----
include::Dockerfile[lines=9..12]
----

For instance:

    docker run --rm -it -e MINIKUBE_API_HOST=maxikube --network minikube --name minikube-proxy chevdor/nginx-minikube-proxy

== Troubleshooting

If you see:

    ...
    2021/02/08 13:36:43 [emerg] 10#10: host not found in upstream "minikube" in /etc/nginx/nginx.conf:15
    nginx: [emerg] host not found in upstream "minikube" in /etc/nginx/nginx.conf:15

It means minikube is not working or not in the network you provided with `--network`. The solution is to pass the right network and eventually pass `MINIKUBE_API_HOST` as shown above.
