FROM nginx:stable-alpine
LABEL maintainer="chevdor"

WORKDIR /template

COPY nginx.tmpl /template
COPY start.sh /template

ENV LISTEN_PORT=18443
ENV LISTEN_HOST=0.0.0.0
ENV MINIKUBE_API_HOST=minikube
ENV MINIKUBE_API_PORT=8443

CMD ["/template/start.sh"]
